# Ouvrir un terminal



## Qu'est-ce que le terminal?

Le terminal s'ouvre en général sur une fenêtre sur fond noir dans laquelle on entrera des commandes.

## Ouvrir un terminal

Vous pouvez ouvrir un terminal avec le raccourci Ctrl + Alt + T.

## Lancer python

Notre première utilisation du terminal consistera à faire quelques tests du langage python.

Entrez simplement dans le terminal:

```
$ python3
```


!!! info
    Le symbole $ ne doit pas être entré, ce symbole -- ou un autre du même genre -- apparaît 
    déjà en général, on parle d' **invite de commandes**.
    [Pour aller plus loin](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/38076-entrer-une-commande).





Une session python s'ouvre alors. Vous devriez notamment voir la version de python installée sur votre machine.

En ce qui me concerne, il s'agit de la version 3.7.1

```
Python 3.7.1
```


## Quelques essais simples



Faites quelques essais simples. Appuyez sur la touche "Entrée" à la fin d'une ligne: cela a pour conséquence d'afficher
le résultat.

```
>>> 2 + 3
5
>>> 8 * 9
72
```

Cet affichage immédiat du résultat  explique l'un des usages de la console: tester rapidement et efficacement
l'effet d'une instruction.


## Instruction sans afficher le résultat

Si vous voulez entrer une instruction sans afficher le résultat immédiatement, appuyer sur la touche **⇧** en même temps
que la touche "entrée".

Testez en tentant de reproduire ceci:

```
>>> a = 2 + 3
>>> a = a + 4
>>> a
9
```



## Remonter dans l'historique

Avec la flèche haut du clavier, les entrées précédemment testées sont réécrites dans la ligne courante. 
Vous pouvez alors   modifier,   retester...

Essayez!


## Sortir de l'interpréteur python

Pour terminer votre session python,  appuyez sur `Ctrl + D`, la main est normalement redonnée à la console linux.

Vous pouvez également terminer votre session python en entrant `quit()`.


