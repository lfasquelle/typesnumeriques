# Les flottants


## Nombre décimal

On utilise les flottants 
pour représenter   les nombres réels.

Le nombre de chiffres utilisé en machine est nécessairement limité: une machine ne connaît donc
pas tous les décimaux (elle ne connaît pas les décimaux ayant "trop" de chiffres).

Comme une infinité de nombres réels n'ont même pas un nombre fini de chiffres, il est clair qu'aucun de ces réels
n'est représenté par un flottant.

Les nombres $\sqrt{2}$, $\pi$ sont par exemple impossible à manipuler dans un langage de programmation 
(que ce soit python, java, javascript, C, ...).
Seules des approximations de ces nombres sont connues.

Il existe donc une infinité de nombres non représentés en machine. L'essentiel des calculs en machine ne sont donc
que des calculs de valeurs approchées. 



## Des approximations parfois étonnantes

Au problème du nombre fini de chiffres s'ajoute le problème de la représentation des nombres en base 2 
(nous y reviendrons en détail).

Cela donne des résultats dont il faut apprendre à se méfier. 

Demandons à Python si 0.1 + 0.1 est égal à 0.2 (on utilise pour cela == )

```
>>> 0.1 + 0.1 == 0.2
True
```

Tout semble bien se passer.

Et demandons lui maintenant si 0.1 + 0.1 + 0.1  est égal à 0.3:

```
>>> 0.1 + 0.1 + 0.1 == 0.3
False
```

De même:

```
>>> 0.1 + 0.2 == 0.3
False
```

Python répond FAUX!



Attention, cela n'est pas lié au langage python. On retrouvera ce problème dans tous les langages. 
Le problème est dû à la façon dont les nombres sont codés en machine.

!!! info
    On retiendra qu'on ne doit jamais utiliser == entre deux flottants.
    On retiendra l'exemple ci-dessus de 0.1 + 0.1 + 0.1.
    

!!! info
    Si l'on cherche à tester l'égalité de flottants en machine, on ne teste que leur proximité
    (avec risques d'erreur donc).
    Python propose une fonction spécifique pour ce type de test: 
    [isclose](https://docs.python.org/fr/3/library/math.html#math.isclose).
    
    
    
## Des règles de calcul distinctes de celles des réels

!!! info
    Soient a, b, c des nombres réels.
    On a: $(a+b)+c = a+(b+c)$.
    C'est ce que l'on appelle **associativité de l'addition** de  nombres réels
    *(le mot associativité est utilisé pour rappeler que l'on peut, pour additionner, "associer" les deux premiers
    ou "associer" les deux derniers)*.
    
    
    
L'addition des flottants **n'est pas associative**:

```
>>> (0.3 + 0.9) + 0.2 == 0.3 + (0.9 + 0.2)
False
```

On insiste donc, une fois de plus, sur la non-utilisation de `==` entre flottants: l'information obtenue avec
un test `a == b` (où a et b sont de type float)
ne concerne que les flottants  et donne rarement une information sur
les nombres mathématiques qu'ils sont censés représenter.



## Opérations sur les flottants


```
>>> type(2.1 + 3.2) # addition
<class 'float'>
>>> type(2.0 * 3.0) # multiplication
<class 'float'>
>>> type(2.0 ** 3.0) # exposant
<class 'float'>
>>> type(3.0 / 5.1) # division
<class 'float'>
>>> type(3.1 - 5.7) # soustraction
<class 'float'>
```


## Erreur en programmation 


En apprenant à programmer, on passe beaucoup de temps à débuguer.
Il faut apprendre à repérer les erreurs, notamment les plus fréquentes.

En voici une:

```
>>> 3.0 / 0.0
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: float division by zero
```


## Opération mixte

On a vu que les flottants et les entiers ne sont pas codés de la même façon.
Une conséquence est qu'on ne devrait pas pouvoir faire d'opérations entre un entier et un flottant.

Essayons:

```
>>> 3.1 + 2
5.1
>>> type(3.0 + 2)
<class 'float'>
```

En fait, dans une situation comme celle-ci, python transforme l'entier en flottant (2 devient 2.0) 
puis effectue l'addition.

!!! info
    Dans certains langages de programmation, une tentative d'opération comme la précédente entre 
    un flottant et un entier ménerait à une erreur.
